/*
 * @Description: 
 * @Version: 2.0
 * @Author: AICHEN
 * @Date: 2022-12-10 16:51:48
 * @LastEditors: AICHEN
 * @LastEditTime: 2022-12-10 18:03:05
 */
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import 'uno.css'
import App from './App.vue'
import router from './router'
import '@/assets/css/reset.css'


import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(ElementPlus)

app.mount('#app')
