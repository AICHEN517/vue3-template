/*
 * @Description: 
 * @Version: 2.0
 * @Author: AICHEN
 * @Date: 2022-12-10 16:51:48
 * @LastEditors: AICHEN
 * @LastEditTime: 2022-12-11 22:06:36
 */
import { defineStore } from "pinia" // 定义容器

export const useThemeStore = defineStore('useThemeStore', {
    /**
     * 存储全局状态
     * 1.必须是箭头函数: 为了在服务器端渲染的时候避免交叉请求导致数据状态污染
     * 和 TS 类型推导
    */
    state: () => ({
        BackgroundColor: "bg-blue-100"
    }),
    /**
     * 用来封装计算属性 有缓存功能  类似于computed
     */
    getters: {

    },
    /**
     * 编辑业务逻辑  类似于methods
     */
    actions: {

    }

})