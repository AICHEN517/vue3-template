
/*
 * @Description: 
 * @Version: 2.0
 * @Author: AICHEN
 * @Date: 2022-12-10 19:51:51
 * @LastEditors: AICHEN
 * @LastEditTime: 2022-12-14 13:08:52
 */
 interface ApiResponse<T> {
	//基本上响应数据里的message都是string类型
	msg: string;
	//但是data的类型是变化的，所以我们不能写死，需要传给axios
	data: T;
	// 响应码
	code: number | string;
}

interface WorseProblem{
	subject:string,
	id:string
}
interface Options {
    val: string, key: string
}

interface loacalUseProblem {
	id?:string,
    subject: string,
    desc: string,
    options: Array<Options>,
    answer: Array<string>
}

interface problemFormExtendType{
    data1:loacalUseProblem,
    data2:loacalUseProblem,
    vis:boolean,
}



export type{ApiResponse,WorseProblem,Options,loacalUseProblem,problemFormExtendType}