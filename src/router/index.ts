/*
 * @Description:
 * @Version: 2.0
 * @Author: AICHEN
 * @Date: 2022-12-10 16:51:48
 * @LastEditors: AICHEN
 * @LastEditTime: 2022-12-22 10:32:13
 */
import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layout/index.vue'
import BaseIndex from '@/components/basic/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      component:Layout,
      path:'/home',
      children:[
        {
          component:BaseIndex,
          name:"BaseIndex",
          path:'base'
        }
      ]
    }
  ]
})

export default router
