
/*
 * @Description: 

 * @Version: 2.0
 * @Author: AICHEN
 * @Date: 2022-12-12 14:46:07
 * @LastEditors: AICHEN
 * @LastEditTime: 2022-12-12 14:46:42
 */
const nextId = (): string => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


export {nextId}