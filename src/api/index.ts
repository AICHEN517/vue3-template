/*
 * @Description: 
 * @Version: 2.0
 * @Author: AICHEN
 * @Date: 2022-12-10 16:51:48
 * @LastEditors: AICHEN
 * @LastEditTime: 2022-12-14 11:33:40
 */
import axios from 'axios';
import { ElMessage } from 'element-plus';
import router from '@/router';
import type { Method, AxiosRequestConfig } from 'axios';
import type {ApiResponse} from '@/types/index';


// 创建实例
const service = axios.create({
	// 后端接口地址
	baseURL: import.meta.env.VITE_API,
	// 请求超时时间
	timeout: 8000,
	headers: {
		'Content-Type': 'application/json;charset=utf-8',
	},
});

//请求拦截
service.interceptors.request.use((config) => {

	return config;
});

// 响应拦截
service.interceptors.response.use(
	(res) => {
		const code: number = res.data.code;

        if (code >= 4000) {
			// 弹窗提示
			// ElMessage.error(res.data.msg);
			// 错误数据交给后续处理
			console.log(res.data);
			return Promise.reject(res.data);
		}
		return Promise.resolve(res.data);
	},
	(err) => {
		// 这里用来处理http常见错误，进行全局提示
		let message = '';
		// 这里错误消息可以使用全局弹框展示出来
		// 比如element plus 可以使用 ElMessage
		ElMessage({
			showClose: true,
			message: `${message} 请检查网络或联系管理员！`,
			type: 'error',
		});
		// 这里是AxiosError类型，所以一般我们只reject我们需要的响应即可
		return Promise.reject(err.response);
	}
);

const request = <T>(url: string, method: Method, submitData?: object | string, config?: AxiosRequestConfig) => {
    console.log("正在发起请求",url);
	return service.request<T, ApiResponse<T>>({
		url,
		method,
		[method.toLowerCase() === 'get' ? 'params' : 'data']: submitData,
		// 解构剩余配置
		...config,
	});
};

export default request;
