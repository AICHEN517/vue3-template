/*
 * @Description: 
 * @Version: 2.0
 * @Author: AICHEN
 * @Date: 2022-12-10 16:51:48
 * @LastEditors: AICHEN
 * @LastEditTime: 2022-12-11 14:52:19
 */

import { defineConfig,loadEnv  } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import unocss from 'unocss/vite'
import path from 'path';
// https://vitejs.dev/config/
export default defineConfig(
  ({ mode }) => {
    const env = loadEnv(mode, __dirname)
    return {
      plugins: [vue(), vueJsx(), unocss()],
      base: env.VITE_MODE === 'production' ? '/' : './',
      resolve: {
        alias: {
          '@': path.resolve(__dirname, './src'),
        }
      },
      define: {
        'process.env': process.env
      }
    }
  })
